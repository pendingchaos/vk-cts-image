Vulkan CTS image
================

Easy to use docker container for running the CTS and fossilize with RADV.

Usage example
-------------

Running the CTS using mesa master on 12 threads with a Fiji GPU (such as R9 Nano):

```
./run-cts.sh -r hakzsam -b master -g fiji -j 12
```

For testing the main mesa project, replace hakzsam by origin.

External sources
----------------

Some external sources are required, especially a mesa local copy which should
be cloned the first time.

```
./external/fetch_sources
```

Required dependencies
---------------------

* docker
* git-lfs

Various issues
--------------

### Permission denied

Shouldn't happen anymore, but if it does, disabling SELinux helps:

```
setenforce 0
```

### Docker fails to start properly with systemd

Might happen on older systemd and/or docker releases.
The following kernel parameter will workaround it:

```
systemd.unified_cgroup_hierarchy=0
```
